import { StackScreenProps } from "@react-navigation/stack";
import { Box, Heading, HStack, Image, NativeBaseProvider, ScrollView, Text, VStack } from "native-base";
import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";
import { RootStackParamList } from "../routes";
import colors from "../styles/colors";


type Props = StackScreenProps<RootStackParamList, 'Details'>;

export default function Details({ route, navigation }: Props) {
    const [item, setItem] = useState({
        "id": "",
        "title": "",
        "description": "",
        "authors": [
            ""
        ],
        "pageCount": 0,
        "category": "",
        "imageUrl": undefined,
        "isbn10": "",
        "isbn13": "",
        "language": "",
        "publisher": "",
        "published": 0
    });

    useEffect(() => {
        setItem(route.params.data)
    }, []);

    return (
        <NativeBaseProvider>
            <Box
                safeArea
                style={styles.container}
            >
                 {/* <HStack alignItems='center'>
                    <IconButton
                        onPress={() => navigation.goBack()}
                        style={styles.iconBtn}
                        icon={<Icon size="md" as={<MaterialIcons name="keyboard-backspace" size={24} color="black" />} />}
                    />
                </HStack> */}
                <ScrollView showsVerticalScrollIndicator={false}>
                    <VStack>
                        <Image
                            source={{
                                uri: item.imageUrl,
                            }}
                            alt="Alternate Text"
                            resizeMode="contain"
                            alignSelf={'center'}
                            size={500}
                        />
                        <Heading mt={15}>{item.title}</Heading>
                        <Text color={colors.link}>{item.authors.join(', ')}</Text>
                    </VStack>
                    <VStack space={'sm'}>
                        <Heading mt={16} mb={5} size={'md'}>INFORMAÇÕES</Heading>
                        <HStack justifyContent={'space-between'}>
                            <Text bold>Páginas</Text>
                            <Text color={colors.gray}>{item.pageCount} páginas</Text>
                        </HStack>
                        <HStack justifyContent={'space-between'}>
                            <Text bold>Editora</Text>
                            <Text color={colors.gray}>{item.publisher}</Text>
                        </HStack>
                        <HStack justifyContent={'space-between'}>
                            <Text bold>Publicação</Text>
                            <Text color={colors.gray}>{item.published}</Text>
                        </HStack>
                        <HStack justifyContent={'space-between'}>
                            <Text bold>Idioma</Text>
                            <Text color={colors.gray}>{item.language}</Text>
                        </HStack>
                        <HStack justifyContent={'space-between'}>
                            <Text bold>Título Original</Text>
                            <Text color={colors.gray}>{item.title}</Text>
                        </HStack>
                        <HStack justifyContent={'space-between'}>
                            <Text bold>ISBN-10</Text>
                            <Text color={colors.gray}>{item.isbn10}</Text>
                        </HStack>
                        <HStack justifyContent={'space-between'}>
                            <Text bold>ISBN-13</Text>
                            <Text color={colors.gray}>{item.isbn13}</Text>
                        </HStack>
                        <HStack justifyContent={'space-between'}>
                            <Text bold>Categoria</Text>
                            <Text color={colors.gray}>{item.category}</Text>
                        </HStack>
                    </VStack>
                    <VStack>
                        <Heading mt={16} mb={5} size={'md'}>RESENHA DA EDITORA</Heading>
                        <HStack>
                            <Text color={colors.gray}>{item.description}</Text>
                        </HStack>
                    </VStack>
                </ScrollView>
            </Box>
        </NativeBaseProvider>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 40,
        paddingVertical: 20,
        width: '100%',
        backgroundColor: '#FFFFFF'
    },
    card: {
        width: "100%",
        marginTop: 15,
        marginBottom: 10,
        padding: 15,
        borderRadius: 5,
        backgroundColor: '#FFF'
    },
    text: {
        color: "white",
        fontSize: 42,
        lineHeight: 84,
        fontWeight: "bold",
        textAlign: "center",
        backgroundColor: "#000000c0"
    },
    iconBtn: {
        backgroundColor: 'white',
        borderColor: '#e2e2e2',
        borderRadius: 30,
        width: 50,
        height: 50,
        borderWidth: 1,
    }
});