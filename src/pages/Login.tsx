import AsyncStorage from '@react-native-async-storage/async-storage';
import { StackScreenProps } from '@react-navigation/stack';
import { Box, Button, Center, FormControl, Heading, HStack, Input, NativeBaseProvider, Text, Toast, VStack } from 'native-base';
import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet } from 'react-native';
import { RootStackParamList } from '../routes';
import usuarioService from '../services/UsuarioService';
import colors from '../styles/colors';

type Props = StackScreenProps<RootStackParamList, 'Login'>;

export default function Login({ route, navigation }: Props) {
    const [email, setEmail] = useState('desafio@ioasys.com.br')
    const [password, setPassword] = useState('12341234')
    const [isLoading, setLoading] = useState(false)
    const [isLoadingToken, setLoadingToken] = useState(false)

    const login = () => {
        setLoading(true)
        let data = {
            email: email,
            password: password
        }
        usuarioService.login(data)
            .then((response) => {
                setLoading(false)
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Home' }]
                })
                console.log("Acesso realizado com sucesso!")
            }).catch((error) => {
                setLoading(false)
                Toast.show({title: 'Não foi possível fazer o login, tente novamente!'})
            })
    }

    const loginToken = (token: any) => {
        setLoadingToken(true)

        usuarioService.loginComToken(token)
            .then((response) => {
                setLoadingToken(false)
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Home' }]
                })                
                console.log("Acesso realizado com sucesso!")
            }).catch((error) => {
                setLoadingToken(false)
            })
    }

    useEffect(() => {
        AsyncStorage
            .getItem('token')
            .then((token) => {
                loginToken(token)
            })
            
    }, [])

    return (
        <NativeBaseProvider>
            {!isLoadingToken &&
            <>
            <ImageBackground source={require('../../assets/background-image.png')} resizeMode="cover" style={styles.image} />
            <Center flex={1} >
                <Box
                    safeArea
                    p={2}
                    w='90%'
                    mx='auto'
                >
                    <HStack mt={-20} mb={20}>
                        <Heading
                            size="lg"
                            color='#FFF'
                            fontSize={50}>
                            ioasys
                        </Heading>
                        <Text color='#FFF'
                            ml={15}
                            fontSize={50}>
                            Books
                        </Text>
                    </HStack>
                    <VStack space={10} mt={5}>
                        <FormControl>
                            <Box backgroundColor={'#00000032'} borderRadius={5} p={3}>
                                <FormControl.Label>
                                    <Text color={colors.gray}>Email</Text>
                                </FormControl.Label>
                                <Input
                                    border={0}
                                    color='#FFFFFF'
                                    onChangeText={value => setEmail(value)}
                                    placeholder={'email@example.com'}
                                    defaultValue={'desafio@ioasys.com.br'}
                                    placeholderTextColor={'#FFFFFF99'} />
                            </Box>
                            <Box mt={5} backgroundColor={'#00000032'} borderRadius={5} p={3}>
                                <HStack>
                                    <VStack flex={1}>
                                        <FormControl.Label>
                                            <Text color={colors.gray}>Senha</Text>
                                        </FormControl.Label>
                                        <Input
                                            border={0}
                                            placeholder='********'
                                            defaultValue='12341234'
                                            onChangeText={value => setPassword(value)}
                                            placeholderTextColor={'#FFFFFF99'}
                                            color='#FFFFFF'
                                            type='password' />
                                    </VStack>
                                        <Button isLoading={isLoading} style={styles.btnRounded} onPress={() => login()}>
                                            <Text bold fontSize={20} color={colors.accent}>Entrar</Text>
                                        </Button>
                                </HStack>
                            </Box>
                        </FormControl>
                    </VStack>
                </Box>
            </Center>
            </>
            }
        </NativeBaseProvider>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        //   flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: "center"
    },
    btnRounded: {
        borderRadius: 30,
        marginVertical: 15,
        minWidth: 100,
        marginLeft: 15,
        alignContent: 'center',
        backgroundColor: '#FFF',
    },
    text: {
        color: "white",
        fontSize: 42,
        lineHeight: 84,
        fontWeight: "bold",
        textAlign: "center",
        backgroundColor: "#000000c0"
    }
});