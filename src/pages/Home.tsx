import { MaterialIcons } from "@expo/vector-icons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { StackScreenProps } from '@react-navigation/stack';
import { Box, Button, Heading, HStack, Icon, IconButton, Image, Input, NativeBaseProvider, Text, VStack } from 'native-base';
import React, { useEffect, useState } from 'react';
import { StyleSheet } from "react-native";
import { FlatList } from 'react-native-gesture-handler';
import { RootStackParamList } from '../routes';
import bookService from "../services/BookService";
import colors from '../styles/colors';

type Props = StackScreenProps<RootStackParamList, 'Home'>;

export default function Home({ route, navigation }: Props) {
    const [data, setData] = useState();

    const logout = () => {
        AsyncStorage.removeItem('token')
        navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }]
        })
    }

    useEffect(() => {
        AsyncStorage
            .getItem('token')
            .then((token) => {
                bookService.getBooks(token)
                    .then((response) => {
                        setData(response.data.data)
                    })
            })
    }, []);

    return (
        <NativeBaseProvider>
            <Box
                safeArea
                p={5}
                w='100%'
                mx='auto'
                style={styles.container}
            >
                <HStack mb='10' alignItems='center'>
                    <HStack flex={1}>
                        <Heading
                            size="lg"
                            fontSize={50}>
                            ioasys
                        </Heading>
                        <Text ml={5} fontSize={50}>
                            Books
                        </Text>
                    </HStack>
                    <IconButton
                        onPress={() => logout()}
                        style={styles.iconBtn}
                        icon={<Icon size="md" as={<MaterialIcons name="logout" size={24} color="black" />} />}
                    />
                </HStack>
                <HStack alignItems='center' mb={5}>
                    <Input p={5} borderColor={'#999999'} isDisabled placeholder={'Procure um livro'} flex={1} />
                    <IconButton
                        w={50}
                        h={50}
                        variant='unstyled'
                        icon={<Icon size="md" as={<MaterialIcons name="filter-list" size={24} color="black" />} />}
                    />
                </HStack>
                <FlatList
                    data={data}
                    pagingEnabled
                    renderItem={({ item }) => (
                        <Box style={styles.card}>
                            <Button variant='unstyled' onPress={() => navigation.navigate('Details',{data: item})}>
                                <HStack>
                                    <VStack style={styles.cardImage}>
                                        <Image
                                            source={{
                                                uri: item.imageUrl,
                                            }}
                                            resizeMode={'contain'}
                                            alt="Alternate Text"
                                            size={200}
                                        />
                                    </VStack>
                                    <VStack w={"65%"} justifyContent={'space-between'}>
                                        <><Text bold>{item.title}</Text>
                                        <Text color={colors.link}>{item.authors.join(', ')}</Text></>
                                        <><Text mt={10} color={'#999999'}>{item.pageCount} páginas</Text>
                                        <Text color={'#999999'}>{item.publisher}</Text>
                                        <Text color={'#999999'}>Publicado em {item.published}</Text></>
                                    </VStack>
                                </HStack>

                            </Button>
                        </Box>
                    )}
                    keyExtractor={(item) => item.id} />
            </Box>
        </NativeBaseProvider>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E5E5E5'
    },
    card: {
        width: "100%",
        marginTop: 15,
        marginBottom: 10,
        padding: 15,
        borderRadius: 5,
        backgroundColor: '#FFF'
    },
    cardImage: {
        width: "35%",
        marginLeft: -15,
        marginRight: 15, 
        alignSelf: 'center'
    },
    iconBtn: {
        backgroundColor: 'white',
        borderColor: '#e2e2e2',
        borderRadius: 30,
        width: 50,
        height: 50,
        borderWidth: 1,
    }
});