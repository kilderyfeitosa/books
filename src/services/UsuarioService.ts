import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

class UsuarioService {


    async login(data: any) {
        return axios({
            url: "https://books.ioasys.com.br/api/v1/auth/sign-in",
            method: "POST",
            timeout: 5000,
            data: data,
            headers: {
                Accept: 'application/json'
            }
        }).then((response) => {
            AsyncStorage.setItem("token", response.headers.authorization)
            return Promise.resolve(response)
        }).catch((error) => {
            return Promise.reject(error)
        })
    }

    async loginComToken(token: any) {
        return axios({
            url: "https://books.ioasys.com.br/api/v1/auth/refresh-token",
            method: "POST",
            timeout: 5000,
            // data: data,
            headers: {
                Authorization: "Bearer " + token,
                Accept: 'application/json',
            }
        }).then((response) => {
            AsyncStorage.setItem("token", response.headers.authorization)
            return Promise.resolve(response)
        }).catch((error) => {
            return Promise.reject(error)
        })
    }
}

const usuarioService = new UsuarioService;
export default usuarioService;