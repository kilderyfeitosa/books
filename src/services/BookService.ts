import axios from "axios";

class BookService {
    async getBooks(token: any) {
        return axios({
            url: "https://books.ioasys.com.br/api/v1/books",
            method: "GET",
            timeout: 5000,
            params: {
                page: 1,
                amount: 1000,
                // category: 'biographies'
            },
            // data: data,
            headers: {
                Authorization: "Bearer " + token,
                Accept: 'application/json'
            }
        }).then((response) => {
            return Promise.resolve(response)
        }).catch((error) => {
            return Promise.reject(error)
        })
    }
}
const bookService = new BookService;
export default bookService;