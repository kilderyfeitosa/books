import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { Details, Home, Login } from './pages';

export type RootStackParamList = {
    Home: {token: string};
    Login: undefined;
    Details: {data: any};
  };

const Stack = createStackNavigator<RootStackParamList>();

export default function Routes () {
    return (
        <Stack.Navigator initialRouteName={'Login'} >
            <Stack.Screen
                name='Home'
                component={Home}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name='Login'
                component={Login}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name='Details'
                component={Details}
                options={{ headerShown: true, title: 'Detalhes' }}
            />
        </Stack.Navigator>
    )
}