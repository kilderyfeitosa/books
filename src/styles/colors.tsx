const colors = {
    primary: '#00A85A',
    primaryDark: '#01773F',
    accent: '#B22E6F',
    background: '#E5E5E5',
    gray: '#999999',
    link: '#AB2680',
}

export default colors;