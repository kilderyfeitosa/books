## Dependências utilizadas:

### Expo
    "expo": "~42.0.1",	    
	"expo-status-bar": "~1.0.4",

### Navegação
    "@react-navigation/native": "^6.0.2",
    "@react-navigation/stack": "^6.0.7",
	"react-native-gesture-handler": "~1.10.2",
	
### Estilização
    "native-base": "^3.1.0",

### API
    "axios": "^0.21.4",

# Passo a Passo

É possível realizar um login de um usuário já cadastrado na primeira tela e sendo obrigatório sua autenticação para o acesso a tela principal.

## Aplicação

Optei por utilizar o expo para criação do projeto, pois já tenho experiência em utilizar-lo e facilita bastante no desenvolvimento da aplicação. Sobre os componentes, optei por utilizar a biblioteca Native Base, é possível criar layouts bonitos com muita facilidade, onde seus componentes possuem uma boa estilização, já ajudando a deixar a tela mais bonita e usual para o usuário.

### Tela Login

Os dados para login já estão sendo setados aumaticamente para faciliar o acesso, caso queira testar o aplicativo rapidamente.

### Tela Home

Na tela principal (Home) temos uma listagens de livros, no qual é possível ter uma breve visualização das informações de cada livro, sendo elas: Imagem, Nome do Livro, Nome dos Autores, Quantidade de Páginas, Nome da Editora e data de publicação. Ao clicar no card de um dos itens, terá informações mais detalhadas sobre o livro escolhido. Caso o usuário queria deslogar, basta ele clicar no botão de sair que fica no canto superior direito da tela.  

### Tela Details

Nesta página o usuário consegue visualizar informações mais detalhes sobre o livro selecionado, onde é possível conferir informações como: Categoria, Data de Publicação, Total de Páginas, Editora e até menos uma Resenha da Editora do livro.

## Funcionalidades não realizadas.

Não foi possível criar uma paginação para as listagens dos livros, porém utilizei o Flatlist para ter uma performance amigável e conseguir carregar muitos cards, porém não conseguir diminuir o consumo excessivo, mas está rodando normala tela. Persistencia de login não foi possível concluir a tempo, devido um problema de requisiçao. 
Funcionalidade de buscas e filtros são somente ilustrativos.


## Por fim

Gostei bastante do desafio, muita coisa nova que precisei estudar, por isso não foi possível concluir algumas atividades, pois devido ter alguma coisas novas, busquei estudar sobre o assunto, lendo documentações e fóruns para compreender sobre alguns assunstos que eu não tinha visto antes, Mas foi bem proveitoso, creio que consegui obter mais conhecimento com esse desafio e quero me dedicar cada vez mais. Tive alguns conflitos, alguns erros comuns quando se ta desenvolvendo, mas que no final conseguir contonar e encontrar novas soluções para resolver o problema. 

Agradeço por essa oportunidade, obrigado!
